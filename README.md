# Spaghetti_diffusion

Lets you work with moisture and temperature diffusion across a spaghetti cross section

Requirments

You need python (any version) and the following that is version compatible with the python you use

1) matplotlib - https://matplotlib.org/stable/index.html

2) numpy - https://numpy.org/install/

3) pandas - https://pypi.org/project/pandas/


## Getting started
usage: python 1d.py save true/false g true/false

: save true for plotting the exp vs simulation comparison as shown in the paper.
: save false won't plot this


: g true for using gelatinization criteria. This is irrelevant to the paper
: use g false for reporducing data in the paper


Dcore/Dmoist :  Ratio of diffuson coefficients of inner core to the moist regions on spaghetti cross section

Tg: Glass transition temperature

Diff. Coeff. W:  Diffusion coefficient of moisture in the moist region

Diff. Coeff. T: Thermal diffusivity in the moist region

Both the Diffusion coefficients are in m^2/s

base_T: The room temperature

base_W: Default moisture content in spaghetti 

time (s): Boiling time in seconds


Please email to me at

shilpafrancis@gmail.com

for any queries
