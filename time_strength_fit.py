import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit


def quad_fit(x,a,b):
    return a*x**2+b

def nonlin_fit(x,a, n):
    return a*x**n


def exp_fit(x,a, b):
    return a * 1/(1 +  np.exp(b*x))

def force_calc(x, a, b):
       return a**2 * (1/(1 +  np.exp(b * x))**2)

data = pd.read_csv("./inner_rad_data.csv")
time = data["x"]
rad = data["y"]

#fit,_ = curve_fit(nonlin_fit, time, rad)
fit,_ = curve_fit(exp_fit, time, rad)
a = fit[0]
b = fit[1]
f, (ax1,ax2) = plt.subplots(1,2,figsize=(6,4))
ax1.plot(data["x"], data["y"], 'k', linestyle = 'None', marker= "*", label = 'Sim.')
ax1.plot(data["x"], exp_fit(data["x"],a, b), 'k', label = 'fit')
ax1.set_xlabel("time (min)", fontsize=18)
ax1.set_ylabel("Core Radius", fontsize=18)
ax1.legend()


fitted_force = force_calc(data["x"], a, b)
fit,_ = curve_fit(exp_fit, data["x"], fitted_force)
print("P fit", fit[0],fit[1])
ax2.plot(data["x"], fitted_force, label = "Calculated", marker = "*", markeredgecolor = 'k', linestyle = "None")
ax2.plot(data["x"], exp_fit(data["x"], fit[0], fit[1]), 'k', label = 'exponential fit')
ax2.set_xlabel("time (min)", fontsize=18)
ax2.set_ylabel("$F_{measure}$", fontsize=18)
ax2.legend()
plt.show()
