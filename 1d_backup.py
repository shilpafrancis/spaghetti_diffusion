import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.widgets import Slider, Button
import pandas as pd
from colorama import Fore, Back, Style

def data_extract(filename):
    data = pd.read_csv(filename)
    return data

def diff_vary(D_b,T, Tg):
    D = [0] * len(T)
    indices = []
    for i in range(len(T)):
        if T[i]>Tg:
           indices.append(i) 
           D[i] = D_b  #in m^2/s
        else:
           D[i] = 0.132e-6  #in m^2/s
    try:       
      return D, indices[-1]
    except:
      print(Fore.RED + "Temperature not exceeding glass transition temperature. Increase the 'Time' of boiling")
      print(Fore.RED + "Or reduce glass transition temperature Tg")
      print(Style.RESET_ALL)
      return D, 0
    #else:
    #  return D, indices[-1]  

def diffW_vary(Dw_b,T, Tg):
    Dw = [0] * len(T)
    for i in range(len(T)):
        if T[i]>Tg:
           Dw[i] = Dw_b  #in m^2/s
        else:
           Dw[i] = 0.132e-6  #in m^2/s
    return Dw


def diff_uni(D_b,T):
    D = [0] * len(T)
    for i in range(len(T)):
        if T[i]>0:
           D[i]=0.132e-6
        else:
           D[i]=0.132e-6
    return D     

def exp_temp(time):
    return 0.145 * time + 29.0

def update(val):
    ax1.clear()
    ax2.clear()
    dt = 0.005
    dx = 0.05e-3
    Length = 1.2e-3
    num_seg = int(Length/dx) 
    print("num_seg",num_seg)
    x=np.linspace(0,Length,num_seg) # x axis in m (0 to 1 mm)
    runtime = int(Run_v.val * 60/dt)
    total_time = runtime * dt/60.0 
    print("Total time = ",total_time, " minutes")
    T = [[0]*len(x)] * runtime
    W = [[0]*len(x)] * runtime

    T_uniD = [[0]*len(x)] * runtime
    #D = [0.132e-6] * len(T[0][:]) #initialising diffusion coeff.
    #Dw = [0.132e-6] * len(T[0][:]) #initialising diffusion coeff.
    print("temperature at ", Run_v.val, " min, ", exp_temp(runtime * dt))
    print(runtime)
    for i in range(runtime):
       To = exp_temp(i*dt)
       T[i][0] = To
       W[i][0] = 70
       T_uniD[i][0] = To
       T[i][-1] = T[i][-2]
       #T[i][-1] = 28
       W[i][-1] = W[i][-2]
       T_uniD[i][-1] = T_uniD[i][-2]
    D_b = diffT_v.val * 1e-10
    Dw_b = diffW_v.val * 1e-6
    D, index = diff_vary(D_b,T[i][:], Tg_v.val)
    Dw = diffW_vary(Dw_b, W[i][:], Tg_v.val)
    D_uni = diff_uni(D,T_uniD[i][:])
    for i in range(runtime-1):
       for j in range(1,len(x)-1):
           T[i+1][j] = T[i][j] + (dt/(dx*dx)) * D[j] *  ( T[i][j+1] - 2 * T[i][j] + T[i][j-1])
           W[i+1][j] = W[i][j] + (dt/(dx*dx)) * Dw[j] *  ( W[i][j+1] - 2 * W[i][j] + W[i][j-1]) 
           T_uniD[i+1][j] = T_uniD[i][j] + (dt/(dx*dx)) * D_uni[j] *  ( T_uniD[i][j+1] - 2 * T_uniD[i][j] + T_uniD[i][j-1])
       D, index = diff_vary(D_b,T[i][:], Tg_v.val)
       moist_region = (index + 1) * dx
       Dw = diffW_vary(Dw_b, W[i][:], Tg_v.val)
       D_uni = diff_uni(D,T_uniD[i][:])
       #if index == 0:
       #    break
    for j in range(len(x)):
        W[-1][j] = W[-1][j] + 30
        T[-1][j] = T[-1][j] + 28
    T_plot = list(T[-1])
    T_plot.reverse()
    x_plot = list(x)
    x_plot.reverse()
    W_plot = list(W[-1])
    W_plot.reverse()
    d_2min = data_extract("dried_2min.csv")
    d_4min = data_extract("dried_4min.csv")
    d_6min = data_extract("dried_6min.csv")
    d_8min = data_extract("dried_8min.csv")
    ax1.plot(np.asarray(x)*1e3,T_plot, marker = "*", label ='Temperature') #Show the diffusion profile at the end of runtime
    if abs(Run_v.val - 2) < 0.5:  
      ax1.plot(d_2min["x"],d_2min["y"], label = "Exp. 2min")
    if abs(Run_v.val - 4) < 0.5:  
      ax1.plot(d_4min["x"],d_4min["y"], label = "Exp. 4min")
    if abs(Run_v.val - 6) < 0.5:  
      ax1.plot(d_6min["x"],d_6min["y"], label = "Exp. 6min")
    if abs(Run_v.val - 8) < 0.5:  
      ax1.plot(d_8min["x"],d_8min["y"], label = "Exp. 8min")
    ax1.plot(np.asarray(x)*1e3,W_plot, label = "Moisture")
    #ax1.plot(x*1e3,T_uniD[-1], label = 'uniform diff') #Show the diffusion profile at the end of runtime
    ax1.set_xlabel("x in mm")
    ax1.set_aspect(0.009)
    ax1.legend()
    inner_rad = (Length/1e-3) - moist_region/1e-3
    print("Inner Radius",inner_rad," mm")
    print("Moist region", moist_region/1e-3, " mm")
    c1 = plt.Circle((1.5,1.5),inner_rad, color = 'b')
    c2 = plt.Circle((1.5,1.5),inner_rad + moist_region/1e-3, color = 'g',fill = False)
    ax2.set_xlim((0,3))
    ax2.set_ylim((0,3))
    ax2.add_artist(c1)
    ax2.add_artist(c2)
    ax2.set_aspect(1)
    plt.show()

global ax1
global ax2


f,(ax1,ax2) = plt.subplots(1,2, figsize = (10,10))        
ax1.plot(0,0)
ax_diffT = plt.axes([0.25, 0.05, 0.65, 0.03])
ax_diffW = plt.axes([0.25, 0.1, 0.65, 0.03])
ax_run = plt.axes([0.25, 0.15, 0.65, 0.03])
ax_Tg = plt.axes([0.25, 0.2, 0.65, 0.03])
diffT_v = Slider(ax_diffT, 'Diff. Coeff. T', 0.0, 0.5, 1)
diffW_v = Slider(ax_diffW, 'Diff. Coeff. W', 0.0, 10, 1)
Run_v = Slider(ax_run, 'Time (min)', 0.0, 10, 1)
Tg_v = Slider(ax_Tg, '$T{g}$ (degree C)', 30, 100, 62.5)
diffT_v.label.set_size(18)
diffW_v.label.set_size(18)
Run_v.label.set_size(18)
Tg_v.label.set_size(18)
plt.subplots_adjust(left=0.1,
                    bottom=0.5,
                    right=0.9,
                    top=0.9,
                    wspace=0.4,
                    hspace=1.5)
diffT_v.on_changed(update)
diffW_v.on_changed(update)
Run_v.on_changed(update)
Tg_v.on_changed(update)

plt.figure(1001)
d_2min = data_extract("dried_2min.csv")
plt.plot(d_2min["x"],d_2min["y"])
plt.xlabel("Distance from center")
plt.ylabel("Moist_content %")
d_4min = data_extract("dried_4min.csv")
plt.plot(d_4min["x"],d_4min["y"])
#plt.xlabel("Time (s)")
plt.ylabel("Moist_content %")
d_6min = data_extract("dried_6min.csv")
plt.plot(d_6min["x"],d_6min["y"])
#plt.xlabel("Time (s)")
plt.ylabel("Moist_content %")
d_8min = data_extract("dried_8min.csv")
plt.plot(d_8min["x"],d_8min["y"])
#plt.xlabel("Time (s)")
plt.ylabel("Moist_content %")
plt.show()
