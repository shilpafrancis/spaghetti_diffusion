import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import sys
from matplotlib.widgets import Slider, Button
import pandas as pd
from colorama import Fore, Back, Style
from scipy.optimize import curve_fit

Length = 1.25e-3
dt = 0.005
dx = 0.05e-3
num_seg = int(Length/dx) 
base_W = 10
base_T = 30

def data_extract(filename):
    data = pd.read_csv(filename)
    return data

def exp_fit(x,a, n):
    return a*x**n

def temp_vs_moisture():
   ## Assigning temperature to time in seconds
   # Step 1: Changing the time axis to temperature
   data = data_extract("dried_gm.csv")
   T_axis = []
   for time in data["x"]:
      T_axis.append(exp_temp_moist(time * 60))
   #Step 2: Fitting with respect to temperature    
   a, n = moist_func_T(T_axis, data["y"]) 
   ## Following is a test
   """
   W0 = []
   T_axis = np.asarray(T_axis)
   for i in T_axis:
      W0.append(a*i**n)
   plt.plot(np.asarray(T_axis)+base_T, np.asarray(W0)+base_W)   
   plt.plot(T_axis, data["y"])
   plt.show()
   """
   return a, n
    
    
def diff_vary(D_b,T,W, Tg, gv):
    D = [0] * len(T)
    indices = []
    if sys.argv[4] == "true":
      for i in range(len(T)):
          if W[i]>gv:
             indices.append(i) 
             D[i] = D_b  #in m^2/s
          else:
             D[i] = 0.132e-6
    else:         
      for i in range(len(T)):
        if T[i]>Tg:
           indices.append(i) 
           D[i] = D_b  #in m^2/s
        else:
           D[i] = 0.132e-6  #in m^2/s
    try:       
      return D, indices[-1]
    except:
      print(Fore.RED + "Temperature not exceeding glass transition temperature. Increase the 'Time' of boiling")
      print(Fore.RED + "Or reduce glass transition temperature Tg")
      print(Style.RESET_ALL)
      return D, 0
    #else:
    #  return D, indices[-1]  

def diffW_vary(Dw_b,T, W, Dcore, Tg, gv):
    Dw = [0] * len(T)
    if sys.argv[4] == "true":
       for i in range(len(T)):
          if W[i]>gv:
             Dw[i] = Dw_b  #in m^2/s
          else:
             Dw[i] = Dcore   #in m^2/s
    else:           
       for i in range(len(T)):
          if T[i]>Tg:
             Dw[i] = Dw_b  #in m^2/s
          else:
             Dw[i] = Dcore   #in m^2/s
    return Dw


def diff_uni(D_b,T):
    D = [0] * len(T)
    for i in range(len(T)):
        if T[i]>0:
           D[i]=0.132e-6
        else:
           D[i]=0.132e-6
    return D     

def exp_temp_moist(time):
    return 0.145 * time

def exp_temp(time):
    return 0.145 * time + baseT_v.val

def moist_func_time(filename):
    moist_T = data_extract(filename)
    fit,_ = curve_fit(exp_fit,moist_T["x"],moist_T["y"])
    return fit[0], fit[1]

def moist_func_T(T, W):
    fit,_ = curve_fit(exp_fit,T,W)
    return fit[0], fit[1]

def update(val):
    ax1.clear()
    ax2.clear()
    ax3.clear()
    ax6.clear()
    print("num_seg",num_seg)
    x=np.linspace(0,Length,num_seg) # x axis in m (0 to 1 mm)
    runtime = int(Run_v.val * 60/dt)
    total_time = runtime * dt/60.0 
    print("Total time = ",total_time, " minutes")
    T = [[0]*len(x)] * runtime
    W = [[0]*len(x)] * runtime

    T_uniD = [[0]*len(x)] * runtime
    #D = [0.132e-6] * len(T[0][:]) #initialising diffusion coeff.
    #Dw = [0.132e-6] * len(T[0][:]) #initialising diffusion coeff.
    print("temperature at ", Run_v.val, " min, ", exp_temp(runtime * dt))
    print(runtime)
    a, n = temp_vs_moisture()
    print("Fits", a, n)
    gv = a*Tg_v.val**n
    for i in range(runtime):
       To = exp_temp(i*dt)
       T[i][0] = To
       T[i][1] = T[i][0]
       W[i][0] = a*T[i][0]**n + baseW_v.val
       W[i][1] = W[i][0]
       T[i][-1] = baseT_v.val
       T[i][-2] = T[i][-1]
       W[i][-1] = baseW_v.val
       W[i][-2] = W[i][-1]
       end_point = len(x) - 1
       T_uniD[i][0] = To
       T_uniD[i][-1] = T_uniD[i][-2]
    D_b = diffT_v.val * 1e-6
    Dw_b = diffW_v.val * 1e-10
    Dcore = Dw_b * DR_v.val
    #D, index = diff_vary(D_b,T[i][:], Tg_v.val)
    D = len(x) * [0]
    Dw = len(x) * [0]
    #Dw = diffW_vary(Dw_b, W[i][:], Dcore, Tg_v.val)
    D_uni = diff_uni(D,T_uniD[i][:])
    for i in range(runtime-1):
       for j in range(2,end_point):
           T[i+1][j] = T[i][j] + (dt/(dx*dx)) * D[j] *  ( T[i][j+1] - 2 * T[i][j] + T[i][j-1])
           W[i+1][j] = W[i][j] + (dt/(dx*dx)) * Dw[j] *  ( W[i][j+1] - 2 * W[i][j] + W[i][j-1]) 
           T_uniD[i+1][j] = T_uniD[i][j] + (dt/(dx*dx)) * D_uni[j] *  ( T_uniD[i][j+1] - 2 * T_uniD[i][j] + T_uniD[i][j-1])
       D, index = diff_vary(D_b,T[i][:], W[i][:], Tg_v.val, gv)
       moist_region = (index + 1) * dx
       Dw = diffW_vary(Dw_b, T[i][:], W[i][:], Dcore, Tg_v.val, gv)
       D_uni = diff_uni(D,T_uniD[i][:])
 

    #for j in range(len(x)):
    #    W[-1][j] = W[-1][j] + base_W
             
    T_plot = list(T[-1])
    T_plot.reverse()
    Dw_plot = list(Dw)
    D_plot = list(D)
    D_plot.reverse()
    Dw_plot.reverse()
    x_plot = list(x)
    x_plot.reverse()
    W_plot = list(W[-1])
    W_plot.reverse()
    d_2min = data_extract("dried_2min.csv")
    d_4min = data_extract("dried_4min.csv")
    d_6min = data_extract("dried_6min.csv")
    d_8min = data_extract("dried_8min.csv")
    ax6.plot(np.asarray(x)*1e3,Dw_plot,'g',label = "D Moisture")
    ax6.set_ylabel("D Moist",fontsize=18)
    ax3.plot(np.asarray(x)*1e3,D_plot,'k', label = "D Temp")
    ax3.set_ylabel("D Temp", fontsize=18)
    ax3.set_xlabel("x in mm",fontsize=18)
    ax1.plot(np.asarray(x)*1e3,np.asarray(T_plot), linestyle = 'None', markeredgecolor = 'k', marker = "x", label='$T^{ 0C}$') #Show the diffusion profile at the end of runtime
    if abs(Run_v.val - 2) < 0.5:  
      ax1.plot(d_2min["x"],d_2min["y"], linestyle = 'None', marker = '*', markeredgecolor = 'k', label = "Exp. (moist) 2min")
    if abs(Run_v.val - 4) < 0.5:  
      ax1.plot(d_4min["x"],d_4min["y"], linestyle = 'None', marker = '*', markeredgecolor = 'k', label = "Exp. (moist) 4min")
    if abs(Run_v.val - 6) < 0.5:  
      ax1.plot(d_6min["x"],d_6min["y"], linestyle = 'None', marker = '*', markeredgecolor = 'k', label = "Exp. (moist) 6min")
    if abs(Run_v.val - 8) < 0.5:  
      ax1.plot(d_8min["x"],d_8min["y"], linestyle = 'None', marker = '*', markeredgecolor = 'k', label = "Exp. (moist) 8min")
    ax1.plot(np.asarray(x)*1e3,np.asarray(W_plot), 'k', label = "Moist. %")
    #ax1.plot(x*1e3,T_uniD[-1], label = 'uniform diff') #Show the diffusion profile at the end of runtime
    ax1.set_xlabel("x in mm", fontsize=18)
    #ax1.set_aspect(0.01)
    ax2.set_title("Cross section", fontsize=18)
    ax1.legend()
    ax3.legend()
    ax6.legend()
    inner_rad = (Length/1e-3) - moist_region/1e-3
    ax5.plot(Run_v.val,inner_rad, marker = 'o', markerfacecolor='k')
    ax5.set_xlabel("Time (minutes)", fontsize = 18)
    ax5.set_ylabel("Core Radius (mm)", fontsize = 18)
    print("Inner Radius",inner_rad," mm")
    print("Moist region", moist_region/1e-3, " mm")
    c1 = plt.Circle((1.5,1.5),inner_rad, color = 'b')
    c2 = plt.Circle((1.5,1.5),inner_rad + moist_region/1e-3, color = 'g',fill = False)
    ax2.set_xlim((0,3))
    ax2.set_ylim((0,3))
    ax2.add_artist(c1)
    ax2.add_artist(c2)
    ax2.set_aspect(1)
    plt.subplots_adjust(left=0.1,
                    bottom=0.5,
                    right=0.9,
                    top=0.93,
                    wspace=0.4,
                    hspace=0.4)
    plt.show()

global ax1
global ax2
global ax3
global ax4
global ax5
global ax6
global baseT_v

f,((ax1,ax2,ax3),(ax4,ax5,ax6)) = plt.subplots(2,3, figsize = (10,12))        
#ax4 = ax3.twinx()
ax1.plot(0,0)
ax_diffT = plt.axes([0.25, 0.05, 0.65, 0.03])
ax_diffW = plt.axes([0.25, 0.1, 0.65, 0.03])
ax_run = plt.axes([0.25, 0.15, 0.65, 0.03])
ax_Tg = plt.axes([0.25, 0.2, 0.65, 0.03])
ax_DR = plt.axes([0.25, 0.25, 0.65, 0.03])
ax_baseT = plt.axes([0.25, 0.3, 0.65, 0.03])
ax_baseW = plt.axes([0.25, 0.35, 0.65, 0.03])
diffT_v = Slider(ax_diffT, 'Diff. Coeff. T', 0.0, 0.5, 1)
diffW_v = Slider(ax_diffW, 'Diff. Coeff. W', 0.0, 1000, 400)
Run_v = Slider(ax_run, 'Time (min)', 0.0, 14, 2)
Tg_v = Slider(ax_Tg, '$T{g}$ (degree C)', 30, 100, 62.5)
DR_v = Slider(ax_DR, 'Dcore/Dmoist', 0, 1, 0.05)
baseT_v = Slider(ax_baseT, 'base_T', 0, 100, 30)
baseW_v = Slider(ax_baseW, 'base_W', 0, 100, 10)
diffT_v.label.set_size(18)
diffW_v.label.set_size(18)
Run_v.label.set_size(18)
Tg_v.label.set_size(18)
DR_v.label.set_size(18)
baseT_v.label.set_size(18)
baseW_v.label.set_size(18)
plt.subplots_adjust(left=0.1,
                    bottom=0.5,
                    right=0.9,
                    top=0.9,
                    wspace=0.2,
                    hspace=0.7)
diffT_v.on_changed(update)
diffW_v.on_changed(update)
Run_v.on_changed(update)
Tg_v.on_changed(update)
DR_v.on_changed(update)
baseT_v.on_changed(update)
baseW_v.on_changed(update)

def save_routine():
    save_runtime = [2,4,6,8]   
    W_save = []
    if sys.argv[2] == 'true':
       D_b = 0.05 * 1e-6
       Dw_b = [400e-10, 400e-10, 400e-10, 3000e-10]
       Tg = 62.5
       for r in range(len(save_runtime)):
          Dcore = Dw_b[r] * 0.15
          runtime = int(save_runtime[r] * 60/dt)  
          x=np.linspace(0,Length,num_seg) # x axis in m (0 to 1 mm)
          T = [[0]*len(x)] * runtime
          W = [[0]*len(x)] * runtime
          a, n = temp_vs_moisture()
          gv = a*Tg**n
          for i in range(runtime-1):
             To = exp_temp(i*dt)
             T[i][0] = To
             T[i][1] = T[i][0]
             W[i][0] = a*T[i][0]**n + 15
             W[i][1] = W[i][0]
             T[i][-1] = 30
             T[i][-2] = T[i][-1]
             W[i][-1] = 15
             W[i][-2] = W[i][-1]
          end_point = len(x) - 1
          D, index = diff_vary(D_b,T[i][:], W[i][:], Tg, gv)
          Dw = diffW_vary(Dw_b[r], T[i][:], W[i][:], Dcore, Tg, gv)
          for i in range(runtime-1):
            for j in range(2,end_point):
              T[i+1][j] = T[i][j] + (dt/(dx*dx)) * D[j] *  ( T[i][j+1] - 2 * T[i][j] + T[i][j-1])
              W[i+1][j] = W[i][j] + (dt/(dx*dx)) * Dw[j] *  ( W[i][j+1] - 2 * W[i][j] + W[i][j-1])
            D, index = diff_vary(D_b,T[i][:], W[i][:], Tg, gv)
            moist_region = (index + 1) * dx
            Dw = diffW_vary(Dw_b[r], T[i][:], W[i][:], Dcore, Tg, gv)
          W_save.append(list(np.asarray(W[-1])))  
          print(r,W_save)
       return W_save

"""
## Testing

a, n = moist_func_time("dried_gm.csv")
data = data_extract("dried_gm.csv")
print("Exp. fit", a, n)
plt.figure(2001)
times = [0, 2, 4, 6, 8, 10, 12, 14]
T0 = []
for i in times:
   ts = i * 60 
   T0.append(exp_temp(ts))
moist = []
for t in times:
    moist.append(a*t**n)
plt.plot(times,moist)
plt.plot(data["x"],data["y"])
"""



if sys.argv[2] == 'true':  
  #plt.figure(1001)
  x=np.linspace(0,Length,num_seg) # x axis in m (0 to 1 mm)
  x = np.asarray(x)*1e3
  W_save = save_routine()
  d_2min = data_extract("dried_2min.csv")
  ax4.plot(d_2min["x"],d_2min["y"],linestyle='None',marker="*", markeredgecolor='k', label = "2min exp.")
  W_save[0].reverse()
  ax4.plot(x,W_save[0],'k', linestyle = '--', label = '2min sim.')
  ax4.set_xlabel("Distance from center",fontsize=18)
  ax4.set_ylabel("Moist_content %",fontsize=18)
  """
  d_4min = data_extract("dried_4min.csv")
  plt.plot(d_4min["x"],d_4min["y"],marker="o", markeredgecolor='k', label = "4min exp.")
  W_save[1].reverse()
  plt.plot(x,W_save[1],linestyle = '-.', label = '4min sim.')
  #plt.xlabel("Time (s)")
  plt.ylabel("Moist_content %")
  """
  d_6min = data_extract("dried_6min.csv")
  ax4.plot(d_6min["x"],d_6min["y"],linestyle='None',marker="x", markeredgecolor='k', label = '6min exp.')
  W_save[2].reverse()
  ax4.plot(x,W_save[2],'k', label = '6min sim.')
  #plt.xlabel("Time (s)")
  ax4.set_ylabel("Moist_content %", fontsize=18)
  ax4.set_xlabel("Distance from center",fontsize=18)
  ax4.legend()
  """
  d_8min = data_extract("dried_8min.csv")
  plt.plot(d_8min["x"],d_8min["y"],marker="X")
  W_save[3].reverse()
  plt.plot(x,W_save[3])
  #plt.xlabel("Time (s)")
  plt.ylabel("Moist_content %")
  """
plt.legend()  
plt.show()
